### Function to create a batch type
# param:
#   - name: name of the batch
#   - distribution: batch effect factors distribibution. current available distribution:
#       - "normal": normal distribution (mean = param_A, sd = param_B)
#       - "gamma": gamma distribution (scale = param_A, shape = param_B)
#   - param_A and param_B: parameter to describe the distribution
# return: a batch type object
# example:
#   batch_1 <- create_batch(name = "Lane1", distribution = "normal", param_A = 1, param_B = 1)
#   batch_2 <- create_batch(name = "Lane2", distribution = "normal", param_A = 3, param_B = 1)
create_batch <- function(name, distribution = "normal", param_A = 0, param_B = 1){
  batch <- list()
  batch[["name"]] <- name
  batch[["param_A"]] <- param_A
  batch[["param_B"]] <- param_B
  batch[["distribution"]] <- distribution
  return(batch)
}


### Function to create a set of batch type
# param:
#   - batch_list: a list of batch type. each element of the list must be the result of a call to function create_batch()
# return: a set of batches
# example:
# batch_1 <- create_batch(name = "Lane1", distribution = "normal", param_A = 1, param_B = 1)
# batch_2 <- create_batch(name = "Lane2", distribution = "normal", param_A = 3, param_B = 1)
# batches <- list(); batches[[1]] <- batch_1; batches[[2]] <- batch_2
# batch_set <- create_batch_set(batch_list = batches)
create_batch_set <- function(batch_list){

  if(is.null(batch_list)){
    stop("ERROR: the input parameter of function create_batch_set() is NULL. It should be a list of batches; each element in the list should be created using create_batch()")
  }

  N_batch_type <- length(batch_list)

  batch_names <- batch_list[[1]][["name"]]

  if(N_batch_type>1){
    for(i in 2:N_batch_type){
      batch_names <- c(batch_names, batch_list[[i]][["name"]])
    }
  }

  if(length(unique(batch_names)) < N_batch_type){
    stop("ERROR: in the input parameter of function create_batch_set(), there are batches having the same name (field batch_list[[i]][\"name\"]. Batch name must be unique.")
  }

  names(batch_list) <- batch_names

  return(batch_list)
}


### Function to create the SPARSim input batch parameter for a single experimental condition
# param:
#   - batch_set: result of function create_batch_set()
#   - batch_sample: a character array of length # sample in the current experimental condition. the i-th element in the array contains the name of the batch type to similuate in the i-th sample.
#                   if the i-th element is set to NULL, then the i-th sample is affected by no batch effect
# return: SPARSim input batch parameter for a single experimental condition
# example:
# batch_1 <- create_batch(name = "Lane1", distribution = "normal", param_A = 1, param_B = 1)
# batch_2 <- create_batch(name = "Lane2", distribution = "normal", param_A = 3, param_B = 1)
# batches <- list(); batches[[1]] <- batch_1; batches[[2]] <- batch_2
# batch_set <- create_batch_set(batch_list = batches)
# batch_sample <- c(rep("Lane1", 20),rep("Lane2", 20)) # having 40 samples, the first 20 samples are affected by batch "Lane1", the second 20 samples are affected by batch "Lane2"
# SPARSim_batch_parameter <- create_SPARSim_batch_parameter(batch_set = batch_set, batch_sample = batch_sample)
create_SPARSim_batch_parameter <- function(batch_set, batch_sample){

  SPARSim_batch_parameter <- list()
  SPARSim_batch_parameter[["batch_set"]] <- batch_set
  SPARSim_batch_parameter[["batch_sample"]] <- batch_sample

  return(SPARSim_batch_parameter)
}


### Function to create batch effects factors
# This functions return the batch effect factors. To apply them, please use the function add_batch_effect()
# param:
#   - batch_factor_matrix: matrix to fill with batch factor values (features on rows; samples on columns)
#   - SPARSim_batch_parameter: SPARSim batch parameter
# return: a numeric matrix, having the same dimension and names of batch_factor_matrix. the element [i,j] in the matrix contains the batch effect factos for i-th feature in j-th sample
create_batch_effect_matrix <- function(batch_factor_matrix, SPARSim_batch_parameter){

  # get the set of batches
  batch_set <- SPARSim_batch_parameter[["batch_set"]]

  # get association between sample and batches
  batch_sample <- SPARSim_batch_parameter[["batch_sample"]]

  for(batch in batch_set){

    batch_name <- batch[["name"]] # get batch name
    batch_factors <- batch[["factors"]] # get batch factor values

    cell_id <- batch_sample == batch_name # get cell IDs related to the current batch
    cell_id[is.na(cell_id)] <- FALSE

    print(cell_id)

    print(length(batch_factors))

    #batch_factors_id <- rownames(batch_factor_matrix) # get feature ID

    batch_factor_matrix[, cell_id] <- rep(batch_factors, sum(cell_id)) # fill matrix by column

  }

  return(batch_factor_matrix)

}




compute_batch_effect_factors <- function(N_batch_factors, batch_factors_id, SPARSim_batch_parameter){

  # get the set of batches
  batch_set <- SPARSim_batch_parameter[["batch_set"]]

  batch_effect_factors <- list()

  N_batch <- length(batch_set)

  for(i in 1:N_batch){

    batch_tmp <- batch_set[[i]]

    if(batch_tmp[["distribution"]] == "normal"){
      batch_factors <- rnorm(N_batch_factors, mean = batch_tmp[["param_A"]], sd = batch_tmp[["param_B"]])
      batch_factors[batch_factors<=0] <- batch_tmp[["param_A"]] # to avoid values <= 0, set them to the mean value
      names(batch_factors) <- batch_factors_id
      batch_tmp[["factors"]] <- batch_factors
    }

    if(batch_tmp[["distribution"]] == "gamma"){
      batch_factors <- rgamma(N_batch_factors, scale = batch_tmp[["param_A"]], shape = batch_tmp[["param_B"]])
      batch_factors[batch_factors<=0] <- batch_tmp[["param_A"]]*batch_tmp[["param_B"]] # to avoid values <= 0, set them to the mean value (scale*shape)
      names(batch_factors) <- batch_factors_id
      batch_tmp[["factors"]] <- batch_factors
    }

    batch_set[[i]] <- batch_tmp
  }

  SPARSim_batch_parameter[["batch_set"]] <- batch_set

  return(SPARSim_batch_parameter)
}




### Function to add batch effect
# param:
#   - fragment_matrix: matrix containing the number of fragment for each feature in each sample (features on rows; samples on columns)
#   - batch_effect_factors: matrix containing the batch effect factors as result of function create_batch_effect_factors()
# return: matrix containing the number of fragment for each feature in each sample (features on rows; samples on columns) affected by batch effect
add_batch_effect <- function(fragment_matrix, batch_effect_factors){

  row_id <- rownames(batch_effect_factors)
  col_id <- colnames(batch_effect_factors)

  return(fragment_matrix[row_id, col_id] * batch_effect_factors[row_id, col_id])

}
