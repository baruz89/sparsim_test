#' Create SPARSim simulation parameter
#'
#' Function to create a SPARSim simulation parameter.
#'
#' To simulate N feature (e.g. genes), user must specify N values of gene expression level and gene expression variability in the function input parameters \code{intensity} and \code{variability}, respectively.
#' To simulate M samples (i.e. cells), user must specify M values of sample library size in the function input parameter \code{library_size}.
#'
#' User can optionally specify the names to assign at the single feature and sample to simulate (function input parameters \code{feature_names} and \code{sample_names}, respectively,
#' as well as the name of the experimental condition (function input parameter \code{condition_name}). If the user does not specify such information, the function will set some default values.
#'
#' To simulate T different experimental conditions in a single count table, then T different simulation parameters must be created.
#'
#' @param intensity Array of gene expression values
#' @param variability Array of gene expression variability values
#' @param library_size Array of library size values
#' @param feature_names Array of feature names. It must be of the same length of \code{intensity} array. If NA (default), feature will be automatically named "gene_1", "gene_2", ... "gene_<N>", where N = length(intensity)
#' @param sample_names Array of sample names. It must be of the same length of \code{library_size} array. If NA (defatul), sample will be automatically named "<condition_name>_cell1", "<condition_name>_cell2", ..., "<condition_name>_cell<M>", where M = length(library_size)
#' @param condition_name Name associated to the current experimental condition. If NA (default), it will be set to "cond<l1><l2>", where l1 and l2 are two random letters.
#' @return SPARSim simulation parameter describing one experimental condition
#' @export
SPARSim_create_simulation_parameter <- function(intensity, variability, library_size, feature_names = NA, sample_names = NA, condition_name = NA){

  # assign feature name to "intensity" and "variability"
  if(length(feature_names)==1){ # potential NA
    if(is.na(feature_names)){
      feature_names <- paste0("gene_", c(1:length(intensity)) )
    }
  }
  names(intensity) <- feature_names
  names(variability) <- feature_names

  # assign condition name
  if(is.na(condition_name)){
    condition_name <- paste0("cond_", paste0(sample(LETTERS, size = 2), collapse = "") )
  }

  # assign sample names to "library_size"
  if(length(sample_names)==1){
    if(is.na(sample_names)){
      sample_names <- paste0(condition_name,"_cell",c(1:length(library_size)))
    }
  }
  names(library_size) <- sample_names

  cond_param <- list()
  cond_param$intensity <- intensity
  cond_param$variability <- variability
  cond_param$lib_size <- library_size
  cond_param$name <- condition_name

  return(cond_param)
}
