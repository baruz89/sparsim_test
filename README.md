# SPARSim

SPARSim is an R tool for the simulation of single cell RNA-seq (scRNA-seq) count table.


## Installation

#### Install from GitLab

SPARSim is available on GitLab at https://gitlab.com/sysbiobig/sparsim

To install from GitLab, please use the following commands:
```r
library(devtools)
install_gitlab("sysbiobig/sparsim")
```

#### Install from source package

SPARSim R package can be downloaded at http://sysbiobig.dei.unipd.it/?q=SPARSim

It requires packages *RCpp*, *Matrix*, *scran* and *edgeR* to work.

To install from source, please use the following command:
```r
install.packages("SPARSim_0.9.5.tar.gz", repos = NULL, type = "source")
```

## Getting started

Please browse SPARSim vignettes using the following commands:

```r
library(SPARSim)
browseVignettes("SPARSim")
```

