% Generated by roxygen2: do not edit by hand
% Please edit documentation in R/SPARSim_estimate.R
\name{SPARSim_estimate_library_size}
\alias{SPARSim_estimate_library_size}
\title{Estimate SPARSim "library size" parameter}
\usage{
SPARSim_estimate_library_size(data)
}
\arguments{
\item{data}{raw count data matrix (gene on rows, samples on columns)}
}
\value{
An array of library size values having \code{N_samples} elements (\code{N_samples = ncol(data)})
}
\description{
Function to estimate the library sizes from the samples in \code{data}.
}
\details{
This function is used in \code{SPARSim_estimate_parameter_from_data} to compute SPARSim "library size" parameter, given a real count table as input.
If the count table contains more than one experimental condition, then the function is applied to each experimental conditions.
}
